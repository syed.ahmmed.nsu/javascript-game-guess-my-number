"use strict";

/*

console.log(document.querySelector(".message").textContent);

document.querySelector(".message").textContent = "✌ Correct Guess";

console.log(document.querySelector(".message").textContent);

document.querySelector(".number").textContent = 20;

document.querySelector(".score").textContent = 13;

document.querySelector(".guess").value = 20;
*/

document.querySelector(".check").addEventListener("click", function () {
  const guess = Number(document.querySelector(".guess").value);

  console.log(guess, typeof guess);

  if (!guess) {
    document.querySelector(".message").textContent = "⛔ no number";
  }
});
